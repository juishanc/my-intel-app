FROM node:14 as build
RUN npm install -g gatsby-cli
WORKDIR /app
COPY . .
RUN npm install
RUN gatsby build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /app/public .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
